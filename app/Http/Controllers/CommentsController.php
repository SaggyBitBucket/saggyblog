<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentFormRequest;
use App\Comment;

class CommentsController extends Controller
{
    //
    public function newComment(CommentFormRequest $request){
        $comment = new Comment(array('post_id' => $request->get('post_id'),
        'post_type' => $request->get('post_type'), 'content' => $request->get('content'),
        'user_id' => '0'));
                $comment->save();
                return redirect()->back()->with('status', 'Your comment has been added!');
}
}
