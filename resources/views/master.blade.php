<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <head>
			<title> @yield('title') </title>
			<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" \
			rel="stylesheet" >
			<!-- Include roboto.css to use the Roboto web font, material.css to include \
			the theme and ripples.css to style the ripple effect -->
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/css/roboto.min.css">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/css/material-fullpalette.min.css">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/css/ripples.min.css">
			<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/js/material.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.3.0/js/ripples.min.js"></script>
		</head>
	</head>
<body>
	@include('shared.navbar')
	@yield('content')
	<script src="//code.jquery.com/jquery-1.10.2.min.js" > </script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js" > </script>
	<script src="/js/ripples.min.js" > </script>
	<script src="/js/material.min.js" > </script>
	<script>
	$(document).ready(function() {
	// This command is used to initialize some elements and make them work properly
	$.material.init();
	});
	</script>
</body>
</html>
