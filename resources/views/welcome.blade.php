@extends('master')
@section('title','Home')
@section('content')
<div class="container">
		<div class="row banner">
		<div class="col-md-12">
		<h1 class="text-center margin-top-100 editContent">Learning Laravel 5</h1 >
		<h5 class="text-center margin-top-100 editContent">{!! trans('main.subtitle') !!}</h5>
        @if (session('status'))
            <div class="alert alert-success">
                {{session('status')}}
            </div>
        @endif
		<div class="text-center">
		<img src=" http://learninglaravel.net/img/LearningLaravel5_cover0.png" width=" 302" height=" 391 " alt=" ">
		</div>
		</div>
		</div>
</div>
@endsection
