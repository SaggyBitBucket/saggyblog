@extends('master')
@section('title', 'All users')
@section('content')
<div class="container col-md-8 col-mid-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2>All roles</h2>
            </div>
                @if(session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                
                @endif
                @if($roles->isempty())
                    <p class="alert alert-danger"> There is no role. </p>
                @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{!!$role->name!!}</td>
                                    <td>{!!$role->display_name!!}</td>
                                    <td>{!!$role->description!!}</td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                @endif
            
        </div>
    </div>
@endsection