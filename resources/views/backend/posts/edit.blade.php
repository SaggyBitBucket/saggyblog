@extends('master')
@section('title, Edit A Post')
@section('content')
<div class="container col-md-6 col-md-offset-3">  
    <div class="well well bs-component">  
        <form class="form-horizontal" method="post">
            @foreach ($errors-> all() as $error)
                <p class="alert alert-danger"> {{ $error }} </p>
            @endforeach
            @if(session('status'))
                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
             <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                <fieldset>
                    <legend>Edit a Post</legend>
                        <div class="form-group">
                            <label for="title" class="col-lg-2 control-label"> Title </label>
                                <div class="col-lg-10" >
                                <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}">
                                </div>
                            </div>
                                
                            <div class="form-group">
                                <label for="content" class="col-lg-2 control-label"> Content </label>
                                <div class="col-lg-10" >
                                <textarea class="form-control" name="content" rows="3" id="content">{!! $post->content !!}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="categories" class="col-lg-2 control-label">Categories</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" id="category" name="categories[]" multiple>
                                            @foreach($categories as $category) 
                                                @if(in_array($category->id, $selectedCategories))
                                                    <option value="{!! $category->id !!}"  selected="selected">
                                                    {!! $category->name !!} 
                                                    </option> 
                                                    @else
                                                    <option value="{!! $category->id !!}">
                                                    {!! $category->name !!} 
                                                    </option> 
                                                @endif
                                            @endforeach 
                                        </select>
                                    </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Submit<button>
                                </div>
                            </div>
                </fieldset>
        </form>
     </div>  
</div>
@endsection