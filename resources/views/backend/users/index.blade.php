@extends('master')
@section('title', 'All users')
@section('content')
    <div class="container col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2> All users </h2>
            </div>
            @if(session('status'))
                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
            @if($users -> isEmpty()){
                <p> There is no user </p>
            }
            @endif

            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Joined at</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($users as $user)
                        <tr>
                        <td> {!!$user->id!!} </td>
                        <td> 
                          {!! $user-> name !!}  
                        </td>
                        <td> {!! $user-> email !!}</td>
                        <td> {!! $user-> created_at !!}</td>
                        <td>
                            <a class="btn btn-primary" href="{!! action('Admin\UsersController@edit', $user->id) !!}">edit</a>
                        </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection