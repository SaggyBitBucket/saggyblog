@extends('master')
@section('title', 'Edit a User')
@section('content')
<div class="container col-md-6 col-md-offset-3">
    <div class="well well bs-component">
        @foreach($errors->all() as $error)
                        <p class="alert alert-danger">{{$error}}</p>
                    @endforeach
                    @if(session('status'))
                        <div class="alert alert-success">{{session('status')}}
                        </div>
                    @endif
                    
        <form class="form-horizontal" method="post">
                <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                <fieldset>
                    <legend>Edit a User</legend>
                    
                    <div class="form-group">
                        <label for="name" class="col-lg-2 control-label">Name</label>
                        <div class="col-lg-10">
                        <input type="text" class="form-control" id="name" name="name"
                        value="{!!$user->name!!}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-lg-2 control-label">Email</label>
                        <div class="col-lg-10">
                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="{{$user->email}}}">
                        </div>
                    </div>
                    <div class="form-group">
                    
                        <label for="select" class="col-lg-2 control-label">Role</label>
                        <div class="col-lg-10">
                            <select class="form-control" id="role" name="role[]" multiple>
                            @foreach($roles as $role) 
                            @if(in_array($role, $selectedRoles))
                                <option value="{!! $role->id !!}"  selected="selected">
                                {!! $role->display_name !!} 
                                </option> 
                                @else
                                <option value="{!! $role->id !!}">
                                {!! $role->display_name !!} 
                                </option> 
                            @endif
                            @endforeach 

                            </select>
                        </div>
                        
                    </div>

                    <div class="form-group"> 
                        <label for="password" class="col-lg-2 control-label">Password</label> 
                        <div class="col-lg-10"> 
                        <input type="password" class="form-control" name="password"> 
                        </div> 
                    </div> 
                    <div class="form-group"> 
                        <label for="password" class="col-lg-2 control-label">Confirm password</label> 
                        <div class="col-lg-10"> 
                        <input type="password" class="form-control" name="password_confirmation"> 
                        </div> 
                    </div>
                   
                    <div class="form-group">
                        <div class="col-lg-8 col-lg-offset-2">
                            <button class="btn btn-default" type="reset">Cancel</button>
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                    
                </fieldset>
        </form>

        
    </div>
</div>
@endsection